<?php

use App\Http\Controllers\Api\PassportAuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\DoctorController;
use App\Http\Controllers\Api\PatientController;
use App\Http\Controllers\Api\ScheduleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);

Route::middleware('auth:api')->group(function() {
  Route::group(['prefix' => 'user'], function() {
    Route::put('/update/{user}', [UserController::class, 'update']);
    // Route::delete('/remove/{user}', [UserController::class, 'destroy']);
  });

  Route::group(['prefix' => 'doctor'],function() {
    Route::post('/create', [DoctorController::class, 'store']);
    Route::get('/show/{doctor}', [DoctorController::class, 'show']);
    Route::put('/update/{doctor}', [DoctorController::class, 'update']);
    Route::delete('/remove/{doctor}', [DoctorController::class, 'destroy']);
  });

  Route::group(['prefix' => 'patient'],function() {
    Route::get('/all', [PatientController::class, 'index']);
    Route::post('/create', [PatientController::class, 'store']);
    Route::get('/show/{patient}', [PatientController::class, 'show']);
    Route::put('/update/{patient}', [PatientController::class, 'update']);
    Route::delete('/remove/{patient}', [PatientController::class, 'destroy']);
  });

  Route::group(['prefix' => 'schedule'],function() {
    Route::get('/all', [ScheduleController::class, 'index']);
    Route::post('/create', [ScheduleController::class, 'store']);
    Route::get('/show/{schedule}', [ScheduleController::class, 'show']);
    Route::put('/update/{schedule}', [ScheduleController::class, 'update']);
    Route::delete('/remove/{schedule}', [ScheduleController::class, 'destroy']);
  });
});


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

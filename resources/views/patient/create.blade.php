@extends('layouts.app', ["current" => "patients"])

@section('content')
    <div class="card-body">
        <form action="/patients" method="POST">
            @csrf
            <div class="row">
                <div class="col">
                    <label for="first_name">Nome</label>
                    <input type="text" class="form-control {{$errors->has('first_name')? 'is-invalid' : ''}} " name="first_name" id="first_name" 
                    value="{{ old('first_name') }}">
                    
                    @if ($errors->has('first_name'))
                        <div class="invalid-feedback">
                            {{ $errors->first('first_name')}}
                        </div>
                    @endif
                </div>
                <div class="col">
                    <label for="last_name">Sobrenome</label>
                    <input type="text" class="form-control {{$errors->has('last_name')? 'is-invalid' : ''}} " name="last_name" id="last_name" 
                    value="{{ old('last_name') }}">
                    
                    @if ($errors->has('last_name'))
                        <div class="invalid-feedback">
                            {{ $errors->first('last_name')}}
                        </div>
                    @endif
                </div>
            </div>

            <div class="row" style="margin-top:20px">
                <div class="col">
                    <label for="phone_number">Phone</label>
                    <input type="text" class="form-control {{$errors->has('phone_number')? 'is-invalid' : ''}} " name="phone_number" id="phone_number" 
                    value="{{ old('phone_number') }}" maxLength="11">
                    
                    @if ($errors->has('phone_number'))
                        <div class="invalid-feedback">
                            {{ $errors->first('phone_number')}}
                        </div>
                    @endif
                </div>

                <div class="col">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control {{$errors->has('cpf')? 'is-invalid' : ''}} " name="cpf" id="cpf" 
                    value="{{ old('cpf') }}" maxLength="11">
                    
                    @if ($errors->has('cpf'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpf')}}
                        </div>
                    @endif
                </div>
            </div>

            <div class="row" style="margin-top:20px">
                <div class="col">
                  <label for="birth_date">Data Nascimento</label>
                  <input type="date" class="form-control {{$errors->has('birth_date')? 'is-invalid' : ''}} " name="birth_date" id="birth_date" 
                  value="{{ old('birth_date') }}">
                  
                  @if ($errors->has('birth_date'))
                      <div class="invalid-feedback">
                          {{ $errors->first('birth_date')}}
                      </div>
                  @endif
                </div>
            </div>

            {{-- <div class="form-group">

                <div class="col-md-6">
                    <label for="first_name">Nome</label>
                    <input type="text" class="form-control {{$errors->has('first_name')? 'is-invalid' : ''}} " name="first_name" id="first_name" 
                    value="{{ old('first_name') }}" placeholder="(99)99999-9999" data-mask="(00) 00000-0000">
                    
                    @if ($errors->has('phone'))
                        <div class="invalid-feedback">
                            {{ $errors->first('phone')}}
                        </div>
                    @endif

                </div>
            </div> --}}

            {{-- <div class="form-group">
                <label for="gender">Gênero</label>
                <select class="form-control {{$errors->has('gender')? 'is-invalid' : ''}}" name="gender" 
                id="gender" value="{{ old('gender') }}">
                    <option selected>Selecione...</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Feminino">Feminino</option>
                </select>

                @if ($errors->has('gender'))
                    <div class="invalid-feedback">
                        {{ $errors->first('gender')}}
                    </div>
                @endif
            </div> --}}


            {{-- <div class="form-group">
                <label for="cpf">CPF</label>
                <input type="text" class="form-control cpf {{$errors->has('cpf')? 'is-invalid' : ''}}" name="cpf" id="cpf" 
                  placeholder="000.000.000-00" data-mask="000.000.000-00" value="{{old('cpf')}}">

                @if ($errors->has('cpf'))
                    <div class="invalid-feedback">
                        {{ $errors->first('cpf')}}
                    </div>
                @endif
            </div> --}}

            <!--
            <div class="form-group">
                <label for="travel">Viagem</label>
                <select class="form-control {{$errors->has('travel')? 'is-invalid' : ''}}" name="travel" 
                id="travel" value="{{old('travel')}}">
                    <option selected>Selecione...</option>
                    <option value="Sim">Sim</option>
                    <option value="Não">Não</option>
                </select>

                @if ($errors->has('travel'))
                    <div class="invalid-feedback">
                        {{ $errors->first('travel')}}
                    </div>
                @endif
            </div>
        -->

            {{-- <div class="form-group">
                <label for="zip_code">CEP</label>
                <input type="text" class="form-control {{ $errors->has('zip_code')? 'is-invalid' : ''}}" name="zip_code" id="cep" 
                placeholder="CEP" value="{{old('cep')}}"/>
                
            @if ($errors->has('zip_code'))
                <div class="invalid-feedback">
                    {{ $errors->first('zip_code')}}
                </div>
            @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="district">Bairro</label>
                <input type="text" class="form-control {{$errors->has('district')? 'is-invalid' : ''}} " name="district" id="bairro" 
                placeholder="Bairro" value="{{old('district')}}">
                
                @if ($errors->has('district'))
                    <div class="invalid-feedback">
                        {{ $errors->first('district')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="street">Rua</label>
                <input type="text" class="form-control {{$errors->has('street')? 'is-invalid' : ''}}" name="street" id="rua" 
                placeholder="Rua" value="{{old('street')}}">

                @if ($errors->has('street'))
                    <div class="invalid-feedback">
                        {{ $errors->first('street')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="city">Cidade</label>
                <input type="text" class="form-control {{$errors->has('city')? 'is-invalid' : ''}}" name="city" id="cidade" 
                placeholder="Cidade" value="{{old('cidade')}}">

                @if ($errors->has('city'))
                    <div class="invalid-feedback">
                        {{ $errors->first('city')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="state">Estado</label>
                <input type="text" class="form-control {{$errors->has('state')? 'is-invalid' : ''}}" name="state" id="uf" 
                placeholder="Estado" value="{{old('estado')}}">

                @if ($errors->has('state'))
                    <div class="invalid-feedback">
                        {{ $errors->first('state')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="number_house">Número</label>
                <input type="text" class="form-control {{$errors->has('number_house')? 'is-invalid' : ''}}" name="number_house" id="number_house" 
                placeholder="Número" value="{{old('number_house')}}">

                @if ($errors->has('number_house'))
                    <div class="invalid-feedback">
                        {{ $errors->first('number_house')}}
                    </div>
                @endif
            </div> --}}

            <div style="margin-top:30px">
                <a href="/patients" type="cancel" role="buttont" class="btn btn-danger btn-md">Cancelar</a>
                <button type="submit" class="btn btn-primary btn-md">Salvar</button>
            </div>
        </form>
    </div>
    <script src="{{ asset('js/getCep.js') }}" type="text/javascript" async="true" defer></script>   
@endsection

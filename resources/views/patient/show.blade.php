@extends('layouts.app')

@section('content')

<h1 style="text-align: center;margin-bottom: 30px;">Dados do(a) {{ $patient['first_name']}}</h1>

<div class="row">
  <div class="col">
    <strong>Nome</strong>
    <p> {{ $patient['first_name']}}</p>
  </div>

  <div class="col">
    <strong>Sobrenome</strong>
    <p> {{ $patient['last_name']}}</p>
  </div>
</div>

<div class="row">
  <div class="col">
    <strong>Phone</strong>
    <p> {{ $patient['phone_number']}}</p>
  </div>

  <div class="col">
    <strong>CPF</strong>
    <p> {{ $patient['cpf']}}</p>
  </div>
</div>

<div class="row">
  <div class="col">
    <strong>Data de Nascimento</strong>
    <p> {{ date('d/m/Y', strtotime($patient['birth_date']))}}</p>
  </div>
</div>

{{-- <div class="form-group">
    <strong>CEP</strong>
    <p> {{ $candidate->zip_code}}</p>
</div>

<div class="form-group">
    <strong>Bairro</strong>
    <p> {{ $candidate->district}}</p>
</div>

<div class="form-group">
    <strong>Rua</strong>
    <p> {{ $candidate->street}}</p>
</div>

<div class="form-group">
    <strong>Cidade</strong>
    <p> {{ $candidate->city}}</p>
</div>

<div class="form-group">
    <strong>Estado</strong>
    <p> {{ $candidate->state}}</p>
</div>

<div class="form-group">
    <strong>Número</strong>
    <p> {{ $candidate->number_house}}</p>
</div> --}}

<a href="/patients" class="btn btn btn-secondary">Voltar</a>

@endsection

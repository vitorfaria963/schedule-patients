
@extends('layouts.app', ["current"=> "patients"])

@section('content')

  <a href="/patients/create" class="btn btn-sm btn-primary" role="button">Novo Paciente</a>
    <div class="card border">
        <div class="card-body">
            <h5 class="card-title">Cadastro de pacientes</h5>
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Sobrenome</th>
                        <th>Phone</th>
                        <th>CPF</th>
                        <th>Data Nascimento</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                  @if(!empty($patients))
                    @foreach($patients as $patient)
                   
                        <tr>
                            <td> {{ $patient['first_name']}} </td>
                            <td> {{ $patient['last_name']}} </td>
                            <td> {{ $patient['phone_number']}} </td>
                            <td> {{ $patient['cpf'] }} </td>
                            <td> {{ date('d/m/Y', strtotime($patient['birth_date']))}}</td>
                            
                            <td>
                                <a href="/patients/{{$patient['id']}}" class="btn btn-sm btn-outline-danger"><i class="material-icons">remove_red_eye</i></a>
                                <a href="/patients/{{$patient['id']}}/edit" class="btn btn-sm btn-outline-danger"><i class="material-icons">edit</i></a>
                                <form action="/patients/{{$patient['id']}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger" value="remover" style="margin-left: 91px;margin-top: -64px;"><i class="material-icons">delete</i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                  @endif
                </tbody>
            </table>
        </div>
    </div>

    @endsection

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">
                    {{ __('Dashboard') }}
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if (!empty($doctorExists))
                        <a href="{{ route('doctor.index') }}" title="Meu perfil"><i class="material-icons">person</i></a>
                    @else
                        <p class="card-text"> {{ __('Logado com sucesso!') }}</p>
                    @endif

                    @if(empty($doctorExists))
                        <a href="/doctor/create" class="btn btn-primary">Meu Cadastro</a>
                    @else
                        <ul style="list-style: none;text-align: left;">
                            <li style="margin-bottom:15px;">
                                <a href="{{ route('patients.index') }}" title="Pacientes"><i class="material-icons">group</i></a>
                            </li>

                            @if(!empty($patientExists))
                                <li>
                                    <a href="{{ route('schedules.index') }}" title="Agendamentos"><i class="material-icons">schedule</i></a>
                                </li>
                            @endif
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

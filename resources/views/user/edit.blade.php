@extends('layouts.app', ["current" => "user"])

@section('content')
    <div class="card-body">
        <form action="/user/{{$user['id']}}" method="POST">
            @method('PATCH')
            @csrf
            <div class="row">
              <div class="col">
                <label for="user_name">Nome de Usuário</label>
                <input type="text" class="form-control {{$errors->has('user_name')? 'is-invalid' : ''}} " name="user_name" id="user_name" 
                value="{{$user['user_name']}}">
                
                @if ($errors->has('user_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user_name')}}
                    </div>
                @endif
              </div>

              <div class="col">
                <label for="last_name">email</label>
                <input type="email" class="form-control {{$errors->has('email')? 'is-invalid' : ''}} " name="email" id="email" 
                value="{{$user['email']}}">
                
                @if ($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email')}}
                    </div>
                @endif
              </div>
            </div>

            <div class="row" style="margin-top:20px">
              <div class="col-6">
                <label for="password">Senha</label>
                <input type="password" class="form-control {{$errors->has('password')? 'is-invalid' : ''}} " name="password" id="password">
                
                @if ($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password')}}
                    </div>
                @endif
              </div>
            </div>

          <div style="margin-top:30px">
            <a href="/home" type="cancel" role="buttont" class="btn btn-danger btn-md">Cancelar</a>
            <button type="submit" class="btn btn-primary btn-md">Salvar</button>
          </div>
        </form>
    </div>
    <script src="{{ asset('js/getCep.js') }}" type="text/javascript" async="true" defer></script>   
@endsection

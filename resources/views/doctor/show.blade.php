@extends('layouts.app')

@section('content')

<h1 style="text-align: center;margin-bottom: 30px;">Meu perfil</h1>

<div class="row">
  <div class="col">
    <strong>Nome</strong>
    <p> {{ $doctor['first_name']}}</p>
  </div>

  <div class="col">
    <strong>Sobrenome</strong>
    <p> {{ $doctor['last_name']}}</p>
  </div>
</div>

<div class="row">
  <div class="col">
    <strong>Phone</strong>
    <p> {{ $doctor['phone_number']}}</p>
  </div>

  <div class="col">
    <strong>CRM</strong>
    <p> {{ $doctor['crm']}}</p>
  </div>
</div>

{{-- <div class="form-group">
    <strong>CEP</strong>
    <p> {{ $candidate->zip_code}}</p>
</div>

<div class="form-group">
    <strong>Bairro</strong>
    <p> {{ $candidate->district}}</p>
</div>

<div class="form-group">
    <strong>Rua</strong>
    <p> {{ $candidate->street}}</p>
</div>

<div class="form-group">
    <strong>Cidade</strong>
    <p> {{ $candidate->city}}</p>
</div>

<div class="form-group">
    <strong>Estado</strong>
    <p> {{ $candidate->state}}</p>
</div>

<div class="form-group">
    <strong>Número</strong>
    <p> {{ $candidate->number_house}}</p>
</div> --}}

<a href="/doctor" class="btn btn btn-secondary">Voltar</a>

@endsection

@extends('layouts.app', ["current" => "doctor"])

@section('content')
    <div class="card-body">
        <form action="/doctor" method="POST">
            @csrf
            <div class="row">
                <div class="col">
                    <label for="first_name">Nome</label>
                    <input type="text" class="form-control {{$errors->has('first_name')? 'is-invalid' : ''}} " name="first_name" id="first_name" 
                    value="{{ old('first_name') }}">
                    
                    @if ($errors->has('first_name'))
                        <div class="invalid-feedback">
                            {{ $errors->first('first_name')}}
                        </div>
                    @endif
                </div>
                <div class="col">
                    <label for="last_name">Sobrenome</label>
                    <input type="text" class="form-control {{$errors->has('last_name')? 'is-invalid' : ''}} " name="last_name" id="last_name" 
                    value="{{ old('last_name') }}">
                    
                    @if ($errors->has('last_name'))
                        <div class="invalid-feedback">
                            {{ $errors->first('last_name')}}
                        </div>
                    @endif
                </div>
            </div>

            <div class="row" style="margin-top:20px">
                <div class="col">
                    <label for="phone_number">Phone</label>
                    <input type="text" class="form-control {{$errors->has('phone_number')? 'is-invalid' : ''}} " name="phone_number" id="phone_number" 
                    value="{{ old('phone_number') }}" maxlength="11">
                    
                    @if ($errors->has('phone_number'))
                        <div class="invalid-feedback">
                            {{ $errors->first('phone_number')}}
                        </div>
                    @endif
                </div>

                <div class="col">
                    <label for="crm">CRM</label>
                    <input type="text" maxlength="7" class="form-control {{$errors->has('crm')? 'is-invalid' : ''}} " name="crm" id="crm" 
                    value="{{ old('crm') }}">
                    
                    @if ($errors->has('crm'))
                        <div class="invalid-feedback">
                            {{ $errors->first('crm')}}
                        </div>
                    @endif
                </div>
            </div>
            {{-- <div class="form-group">
                <label for="zip_code">CEP</label>
                <input type="text" class="form-control {{ $errors->has('zip_code')? 'is-invalid' : ''}}" name="zip_code" id="cep" 
                placeholder="CEP" value="{{old('cep')}}"/>
                
            @if ($errors->has('zip_code'))
                <div class="invalid-feedback">
                    {{ $errors->first('zip_code')}}
                </div>
            @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="district">Bairro</label>
                <input type="text" class="form-control {{$errors->has('district')? 'is-invalid' : ''}} " name="district" id="bairro" 
                placeholder="Bairro" value="{{old('district')}}">
                
                @if ($errors->has('district'))
                    <div class="invalid-feedback">
                        {{ $errors->first('district')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="street">Rua</label>
                <input type="text" class="form-control {{$errors->has('street')? 'is-invalid' : ''}}" name="street" id="rua" 
                placeholder="Rua" value="{{old('street')}}">

                @if ($errors->has('street'))
                    <div class="invalid-feedback">
                        {{ $errors->first('street')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="city">Cidade</label>
                <input type="text" class="form-control {{$errors->has('city')? 'is-invalid' : ''}}" name="city" id="cidade" 
                placeholder="Cidade" value="{{old('cidade')}}">

                @if ($errors->has('city'))
                    <div class="invalid-feedback">
                        {{ $errors->first('city')}}
                    </div>
                @endif
            </div> --}}

            {{-- <div class="form-group">
                <label for="state">Estado</label>
                <input type="text" class="form-control {{$errors->has('state')? 'is-invalid' : ''}}" name="state" id="uf" 
                placeholder="Estado" value="{{old('estado')}}">

                @if ($errors->has('state'))
                    <div class="invalid-feedback">
                        {{ $errors->first('state')}}
                    </div>
                @endif
            </div> --}}

            <div style="margin-top:30px">
                <a href="/doctor" type="cancel" role="buttont" class="btn btn-danger btn-md">Cancelar</a>
                <button type="submit" class="btn btn-primary btn-md">Salvar</button>
            </div>
        </form>
    </div>
    <script src="{{ asset('js/getCep.js') }}" type="text/javascript" async="true" defer></script>   
@endsection


@extends('layouts.app', ["current"=> "doctor"])

@section('content')
    <div class="card border">
        <div class="card-body">
            <h5 class="card-title">Listagem de Dados</h5>
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Sobrenome</th>
                        <th>Telefone</th>
                        <th>CRM</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($doctorProfile))    
                      <tr>
                          {{-- <td> {{ $candidate->usuario->name}} </td> --}}
                          <td> {{ ucfirst($doctorProfile['first_name'])}} </td>
                          <td> {{ ucfirst($doctorProfile['last_name'])}} </td>
                          <td> {{ $doctorProfile['phone_number'] }} </td>
                          <td> {{ $doctorProfile['crm']}}</td>
                          
                          <td>
                              <a href="/doctor/{{$doctorProfile['id']}}" class="btn btn-sm btn-outline-danger"><i class="material-icons">remove_red_eye</i></a>
                              <a href= "/doctor/{{$doctorProfile['id']}}/edit" class="btn btn-sm btn-outline-danger"><i class="material-icons">edit</i></a>
                              <form action="/doctor/{{$doctorProfile['id']}}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-sm btn-outline-danger" value="remover" style="margin-left: 91px;margin-top: -64px;"><i class="material-icons">delete</i></button>
                              </form>
                          </td>
                      </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    @endsection

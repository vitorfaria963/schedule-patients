@extends('layouts.app', ["current" => "schedules"])

@section('content')
    <div class="card-body">
        <form action="/schedules" method="POST">
            @csrf
            <div class="row">
                <div class="form-group">
                  <label for="patient_id">Paciente</label>
                    <select class="form-control {{$errors->has('patient_id')? 'is-invalid' : ''}} " name="patient_id" 
                      id="patient_id">
                        <option value="">Selecione...</option>
                        @if(count($patients) > 0)
                         
                          @foreach($patients as $patient)
                            <option value="{{ $patient['id']}}">{{ ucfirst($patient['first_name']). ' '.$patient['last_name'] }}</option>
                          @endforeach
                        @endif

                        @if ($errors->has('patient_id'))
                          <div class="invalid-feedback">
                            {{ $errors->first('patient_id')}}
                          </div>
                        @endif
                    </select>
                </div>

                <div class="col" style="margin-top:20px">
                  <label for="schedule_date">Data da Consulta</label>
                  <input type="date" class="form-control {{$errors->has('schedule_date')? 'is-invalid' : ''}} " name="schedule_date" id="schedule_date" 
                  value="{{ old('schedule_date') }}">
                  
                  @if ($errors->has('schedule_date'))
                      <div class="invalid-feedback">
                          {{ $errors->first('schedule_date')}}
                      </div>
                  @endif
                </div>
            </div>

            <div class="row" style="margin-top:20px">
              <div class="col">
                  <label for="appointment_type">Tipo da consulta</label>
                  <input type="text" class="form-control {{$errors->has('appointment_type')? 'is-invalid' : ''}} " name="appointment_type" id="appointment_type" 
                  value="{{ old('appointment_type') }}" placeholder="Consulta de rotina...">
                  
                  @if ($errors->has('appointment_type'))
                      <div class="invalid-feedback">
                          {{ $errors->first('appointment_type')}}
                      </div>
                  @endif
              </div>
            </div>

            <div style="margin-top:30px">
              <a href="/schedules" type="cancel" role="buttont" class="btn btn-danger btn-md">Cancelar</a>
              <button type="submit" class="btn btn-primary btn-md">Salvar</button>
            </div>
        </form>
    </div>
 
@endsection

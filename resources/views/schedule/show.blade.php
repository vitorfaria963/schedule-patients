@extends('layouts.app')

@section('content')
<h1 style="text-align: center;margin-bottom: 30px;">Dados do agendamento de(a) {{ $patient->first_name}}</h1>

<div class="row">
  <div class="col">
    <strong>Nome</strong>
    <p> {{ $patient->first_name }}</p>
  </div>

  <div class="col">
    <strong>Sobrenome</strong>
    <p> {{ $patient->last_name }}</p>
  </div>
</div>

<div class="row">
  <div class="col">
    <strong>Phone</strong>
    <p> {{ $patient->phone_number }}</p>
  </div>

  <div class="col">
    <strong>CPF</strong>
    <p> {{ $patient->cpf }}</p>
  </div>
</div>

<div class="row">
  <div class="col">
    <strong>Data de Nascimento</strong>
    <p> {{ date('d/m/Y', strtotime($patient->birth_date ))}}</p>
  </div>

  <div class="col">
    <strong>Data do agendamento</strong>
    <p> {{ date('d/m/Y', strtotime($schedule->schedule_date)) }}</p>
  </div>
</div>

<div class="row">
  <div class="col">
    <strong>Tipo da consulta</strong>
    <p> {{ $schedule->appointment_type }}</p>
  </div>
</div>

<a href="/schedules" class="btn btn btn-secondary">Voltar</a>

@endsection


@extends('layouts.app', ["current"=> "schedule"])

@section('content')
  <a href="{{ url('schedules/create')}}" class="btn btn-sm btn-primary" role="button">Agendar Consulta</a>
    <div class="card border">
        <div class="card-body">
            <h5 class="card-title">Listagem de Consultas</h5>
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Sobrenome</th>
                        <th>Telefone</th>
                        <th>CPF</th>
                        <th>Data do Agendamento</th>
                        <th>Tipo da Consulta</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($schedules))  
                   
                      @foreach($schedules as $schedule)
                        <tr>
                            <td> {{ strtoupper($schedule['first_name'])}} </td>
                            <td> {{ strtoupper($schedule['last_name'])}} </td>
                            <td> {{ $schedule['phone_number']}} </td>
                            <td> {{ $schedule['cpf'] }} </td>
                            <td> {{ date('d/m/Y', strtotime($schedule['pivot_schedule_date']))}}</td>
                            <td> {{ ucwords($schedule['pivot_appointment_type']) }}</td>
                            
                            <td>
                                <a href="/schedules/{{$schedule['pivot_id']}}" class="btn btn-sm btn-outline-danger"><i class="material-icons">remove_red_eye</i></a>
                                <a href="/schedules/{{$schedule['pivot_id']}}/edit" class="btn btn-sm btn-outline-danger"><i class="material-icons">edit</i></a>
                                <form action="/schedules/{{$schedule['pivot_id']}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger" value="remover" style="margin-left: 91px;margin-top: -64px;"><i class="material-icons">delete</i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    @endsection

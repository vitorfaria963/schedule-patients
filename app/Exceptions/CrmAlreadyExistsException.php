<?php

namespace App\Exceptions;

use Exception;

class CrmAlreadyExistsException extends Exception
{
    protected $message = 'CRM already exists';

    public function render()
    {
        return response()->json([
            'error' => class_basename($this),
            'message' => $this->getMessage(),
        ], 400);
    }
}

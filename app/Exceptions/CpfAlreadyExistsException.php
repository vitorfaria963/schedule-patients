<?php

namespace App\Exceptions;

use Exception;

class CpfAlreadyExistsException extends Exception
{
    protected $message = 'CPF already exists';

    public function render()
    {
        return response()->json([
            'error' => class_basename($this),
            'message' => $this->getMessage(),
        ], 400);
    }
}

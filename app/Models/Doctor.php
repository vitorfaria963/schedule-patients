<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'phone_number',
        'crm',
        'user_id'
    ];

    /**
     * @return HasOne
     */
    public function user() : HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return HasMany
     */
    public function patients() : HasMany
    {
        return $this->hasMany(Patient::class, 'doctor_id', 'id');
    }

     /**
     * @return BelongsToMany
     */
    public function schedules_patient() : BelongsToMany
    {
        return $this->belongsToMany(Patient::class, 'schedules')->withPivot('schedule_date', 'appointment_type', 'id')->orderBy('schedule_date');
    }

}

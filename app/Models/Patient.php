<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Database\Eloquent\Relations\belongsToMany;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'phone_number',
        'birth_date',
        'cpf'
    ];

    /**
     * @return belongsTo
    */
    public function doctor() : belongsTo
    {
        return $this->belongsTo(Doctor::class, 'doctor_id', 'id');
    }

     /**
     * @return BelongsToMany
     */
    public function schedules_doctor() : BelongsToMany
    {
        return $this->belongsToMany(Doctor::class, 'schedules')->withPivot('schedule_date', 'appointment_type', 'id');
    }
}

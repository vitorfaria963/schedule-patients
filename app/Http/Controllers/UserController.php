<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Services\AuthService;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit')->with('user', $user);
    }

    /**
     * @param UserUpdateRequest $request
     * @throws 
     */
    public function update(User $user, UserUpdateRequest $request)
    {
        $input = $request->validated();
        $input['password'] = !empty($input['password']) ? AuthService::encryptPassword($input['password']) : $user['password'];
        try {
            $user->fill($input);

            $user->save();

            Session::flash('success', 'Dados de perfil atualizados!');

            return redirect(route('home'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível atualizar os dados de perfil!');

            return redirect(route('home'));
        }
    }
}

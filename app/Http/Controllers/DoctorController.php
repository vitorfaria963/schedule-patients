<?php

namespace App\Http\Controllers;

use App\Http\Requests\DoctorStoreRequest;
use App\Http\Requests\DoctorUpdateRequest;
use App\Services\DoctorService;
use App\Models\Doctor;
use App\Models\User;
use Session;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    private $doctorService;
    
    /**
     * DoctorService constructor.
     * @param DoctorService $doctorService
    */
    public function __construct(DoctorService $doctorService)
    {
        $this->middleware('auth');
        $this->doctorService = $doctorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctorProfile = $this->doctorService->getDoctorLoggedIn(auth()->user()->id);

        return isset($doctorProfile) ? view('doctor.index')->with('doctorProfile', $doctorProfile->getAttributes()) : view('doctor.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('doctor.create');
    }

    /**
     * @param DoctorStoreRequest $request
     * @throws 
     */
    public function store(DoctorStoreRequest $request)
    {
        $input = $request->validated();
        $input['user_id'] = auth()->user()->id;

        $doctorCrm = Doctor::where('crm', $input['crm'])->exists();

        if (!empty($doctorCrm)) {
            Session::flash('error', 'CRM já existe!');

            return view('doctor.create');
        }

        try {
            // $doctor = $this->doctorService->store($input['first_name'], $input['last_name'], $input['phone_number'], $input['crm']);
            Doctor::create($input);
            
            Session::flash('success', 'Cadastro efetuado com sucesso!');
    
            return redirect(route('doctor.index'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível concluir o cadastro!');

            return redirect(url('doctor.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        $this->authorize('view', $doctor);

        $doctor->load('user');
    
        return view('doctor.show')->with('doctor', $doctor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        $this->authorize('view', $doctor);

        return view('doctor.edit')->with('doctor', $doctor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Doctor $doctor, DoctorUpdateRequest $request)
    {
        $this->authorize('update', $doctor);

        $input = $request->validated();

        try {
            $doctor->fill($input);
    
            $doctor->save();
    
            Session::flash('success', 'Dados atualizados com sucesso!');
    
            return redirect(route('doctor.index'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível atualizar os dados de cadastro!');

            return redirect(route('doctor.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        $this->authorize('destroy', $doctor);

        $doctor->delete();

        Session::flash('success', 'Dados removidos com sucesso!');

        return redirect(route('doctor.index'));
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientStoreRequest;
use App\Http\Requests\PatientUpdateRequest;
use App\Services\PatientService;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * PatientService constructor.
     * @param PatientService $patientService
    */
    public function __construct(PatientService $patientService)
    {
        $this->patientService = $patientService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return all patients created by the doctor logged in
        $patients = $this->patientService->getPatients();

        return $patients;
    }

     /**
     * @param PatientStoreRequest $request
     * @throws 
     */
    public function store(PatientStoreRequest $request)
    {
        $input = $request->validated();

        try {
            $patient = $this->patientService->store($input);
        
            return $patient;
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        $this->authorize('view', $patient);

        return $patient;
    }

     /**
     * @param PatientUpdateRequest $request
     * @throws 
     */
    public function update(Patient $patient, PatientUpdateRequest $request)
    {
        $this->authorize('update', $patient);

        $input = $request->validated();

        try {
            $patient->fill($input);

            $patient->save();

            return response()->json('Patient successfully updated', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $this->authorize('destroy', $patient);

        try {
            $patient->delete();

            return response()->json('Patient successfully deleted', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, UserUpdateRequest $request)
    {
        $input = $request->validated();
        $input['password'] = !empty($input['password']) ? AuthService::encryptPassword($input['password']) : $user['password'];
        
        try {
            $user->fill($input);

            $user->save();

            return response()->json('User successfully updated', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }
}

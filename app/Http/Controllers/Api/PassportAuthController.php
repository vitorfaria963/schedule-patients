<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRegisterRequest;
use App\Http\Requests\AuthLoginRequest;
use App\Services\AuthService;
use App\Models\User;
use Illuminate\Http\Request;

class PassportAuthController extends Controller
{
    private $authService;

    /**
     * PassportAuthController constructor.
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

     /**
     * @param AuthRegisterRequest $request
     * @throws UserHasBeenTakenException
     */
    public function register(AuthRegisterRequest $request)
    {
        $input = $request->validated();
    
        try {
            $user = $this->authService->register($input['user_name'], $input['email'], $input['password']);
    
            return response()->json('User sucessfully created', 201);
        } catch(\Illuminate\Database\QueryException $exception){
            return response()->json($exception, 500);
        }
    }

    /**
     * @param AuthLoginRequest $request
     * @throws LoginInvalidException
     */
    public function login(AuthLoginRequest $request)
    {
        $input = $request->validated();

        try {
            $login = $this->authService->login($input['email'], $input['password']);

            return $login;
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }
}

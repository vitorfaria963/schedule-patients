<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ScheduleStoreRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Services\ScheduleService;
use App\Services\DoctorService;
use App\Models\User;
use App\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    private $scheduleService;
    private $doctorService;
    /**
     * ScheduleService constructor.
     * @param ScheduleService $scheduleService
     * @param DoctorService   $doctorService
    */
    public function __construct(ScheduleService $scheduleService, DoctorService $doctorService)
    {
        $this->scheduleService = $scheduleService;
        $this->doctorService   = $doctorService;
    }

    public function index()
    {
        // Retorna uma listagem de agendamentos baseados no médico que está logado
        $schedules = $this->scheduleService->getSchedules();

        return $schedules;
    }

    /**
     * @param ScheduleStoreRequest $request
     * @throws 
     */
    public function store(ScheduleStoreRequest $request)
    {
        $input = $request->validated();
        $input['doctor_id'] = $this->doctorService->getDoctorLoggedIn(auth()->user()->id)->getAttributes()['id'];
      
        try {
            $schedule = $this->scheduleService->store($input['patient_id'], $input['doctor_id'], $input['schedule_date'], $input['appointment_type']);
            
            return $schedule;
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        $this->authorize('view', $schedule);
        
        return $schedule;
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Schedule $schedule, ScheduleUpdateRequest $request)
    {
        $teste = $this->authorize('update', $schedule);

        $input = $request->validated();

        try {
            $schedule->fill($input);

            $schedule->save();

            return response()->json('schedule successfully updated', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $teste = $this->authorize('destroy', $schedule);
        
        try {
            $schedule->delete();

            return response()->json('Schedule successfully deleted', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json('server error', 500);
        }
    }
}

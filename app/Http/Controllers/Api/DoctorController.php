<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DoctorStoreRequest;
use App\Http\Requests\DoctorUpdateRequest;
use App\Services\DoctorService;
use App\Models\Doctor;
use App\Models\User;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    private $doctorService;
    
    /**
     * DoctorService constructor.
     * @param DoctorService $doctorService
    */
    public function __construct(DoctorService $doctorService)
    {
        $this->doctorService = $doctorService;
    }

    /**
     * @param DoctorStoreRequest $request
     * @throws 
     */
    public function store(DoctorStoreRequest $request)
    {
        $input = $request->validated();
      
        try {
            $doctor = $this->doctorService->store($input['first_name'], $input['last_name'], $input['phone_number'], $input['crm']);
            
            return $doctor;
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        $this->authorize('view', $doctor);
    
        $doctor->load('user');
      
        return $doctor;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Doctor $doctor, DoctorUpdateRequest $request)
    {
        $this->authorize('update', $doctor);

        $input = $request->validated();

        try {
            $doctor->fill($input);

            $doctor->save();

            return response()->json('Profile successfully updated', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        $this->authorize('destroy', $doctor);

        try {
            $doctor->delete();

            return response()->json('Profile successfully deleted', 200);
        } catch(\Illuminate\Database\QueryException $exception) {
            return response()->json($exception, 500);
        }
    }
}

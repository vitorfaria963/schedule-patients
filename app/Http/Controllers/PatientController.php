<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatientStoreRequest;
use App\Http\Requests\PatientUpdateRequest;
use App\Services\PatientService;
use App\Services\DoctorService;
use App\Models\Patient;
use App\Models\Doctor;
use Session;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    private $patientService;
    private $doctorService;
    /**
     * PatientService constructor.
     * @param PatientService $patientService
     * @param DoctorService $doctorService
    */
    public function __construct(PatientService $patientService, DoctorService $doctorService)
    {
        $this->middleware('auth');
        $this->patientService = $patientService;
        $this->doctorService  = $doctorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return all patients created by the doctor logged in
        $patients = $this->patientService->getPatients();
       
        return view('patient.index')->with('patients', $patients);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient.create');
    }

    /**
     * @param PatientStoreRequest $request
     * @throws 
     */
    public function store(PatientStoreRequest $request)
    {
        $input = $request->validated();
        
        $doctor = Doctor::where('user_id', '=', auth()->user()->id)->first();

        $patientCpf = Patient::where('cpf', $input['cpf'])->exists();

        if (!empty($patientCpf)) {
            Session::flash('error', 'CPF ja existe!');

            return view('patient.create');
        }

        try {
            $doctor->patients()->create($input);
        
            Session::flash('success', 'Paciente adicionado com sucesso!');

            return redirect(url('patients'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível adicionar este paciente!');

            return redirect(url('patients'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        $this->authorize('view', $patient);

        return view('patient.show')->with('patient', $patient);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  array  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        $this->authorize('view', $patient);

        return view('patient.edit')->with('patient', $patient);
    }

    /**
     * @param PatientUpdateRequest $request
     * @throws 
     */
    public function update(Patient $patient, PatientUpdateRequest $request)
    {
        $this->authorize('update', $patient);

        $input = $request->validated();

        try {
            $patient->fill($input);

            $patient->save();

            Session::flash('success', 'Paciente atualizado com sucesso!');

            return redirect(url('patients'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível atualizar este paciente!');

            return redirect(url('patients'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  array  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $this->authorize('destroy', $patient);

        // $doctor = Doctor::find($patient->doctor_id);

        try {
            // $patient->schedules_doctor->each->delete();
            $patient->delete();

            Session::flash('success', 'Paciente removido com sucesso!');

            return redirect(url('patients'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível remover este paciente!');

            return redirect(url('patients'));   
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScheduleStoreRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Services\PatientService;
use App\Services\ScheduleService;
use App\Services\DoctorService;
use App\Models\Schedule;
use Session;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    private $scheduleService;
    private $patientService;
    private $doctorService;
    /**
     * ScheduleService constructor.
     * @param ScheduleService $scheduleService
     * @param PatientService $patientService
     * @param DoctorService $doctorService
    */
    public function __construct(ScheduleService $scheduleService, PatientService $patientService, DoctorService $doctorService)
    {
        $this->middleware('auth');
        $this->scheduleService = $scheduleService;
        $this->patientService  = $patientService;
        $this->doctorService   = $doctorService;
    }

    public function index()
    {
        // Retorna uma listagem de agendamentos baseados no médico que está logado
        $schedules = $this->scheduleService->getSchedules();

        return view('schedule.index')->with('schedules', $schedules);
    }

    public function create()
    {
        $patients = $this->patientService->getPatients();
        
        return view('schedule.create')->with('patients', $patients);
    }

    /**
     * @param ScheduleStoreRequest $request
     * @throws 
     */
    public function store(ScheduleStoreRequest $request)
    {
        $input = $request->validated();
        $input['doctor_id'] = $this->doctorService->getDoctorLoggedIn(auth()->user()->id)->getAttributes()['id'];

        try {
            Schedule::create($input);
        
            Session::flash('success', 'Consulta adicionada com sucesso!');

            return redirect(url('schedules'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível adicionar esta consulta!');

            return redirect(url('schedules'));
        }
    }

    public function show(Schedule $schedule)
    {
        $this->authorize('view', $schedule);

        // Traz os dados do agendamento do paciente pelo id do paciente cadastrado neste agendamento
        $patientData = $this->scheduleService->getScheduledPatient($schedule->patient_id);

        return view('schedule.show')->with('schedule', $schedule)->with('patient', $patientData);
    }

    public function edit(Schedule $schedule)
    {
        $this->authorize('view', $schedule);

        // Traz os dados de agendamento do paciente que foi cadastrado e a listagem de todos os pacientes para preencher o select do form
        $patientData = $this->scheduleService->getScheduledPatient($schedule->patient_id);
        $patients    = $this->patientService->getPatients();

        return view('schedule.edit')
                ->with('schedule', $schedule)
                ->with('currentPatient', $patientData)
                ->with('patients', $patients);
    }

     /**
     * @param ScheduleUpdateRequest $request
     * @throws 
     */
    public function update(Schedule $schedule, ScheduleUpdateRequest $request)
    {
        $this->authorize('update', $schedule);

        $input = $request->validated();

        try {
            $schedule->fill($input);

            $schedule->save();

            Session::flash('success', 'Consulta atualizada com sucesso!');

            return redirect(url('schedules'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível atualizar esta consulta!');

            return redirect(url('schedules'));
        }
    }

    public function destroy(Schedule $schedule)
    {
        $this->authorize('destroy', $schedule);

        try {
            $schedule->delete();

            Session::flash('success', 'Agendamento removido com sucesso!');

            return redirect(url('schedules'));
        } catch(\Illuminate\Database\QueryException $exception) {
            Session::flash('error', 'Não foi possível remover esta consulta!');

            return redirect(url('schedules'));
        }
    }
}

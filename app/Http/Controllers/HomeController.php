<?php

namespace App\Http\Controllers;

use App\Services\DoctorService;
use App\Services\PatientService;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private $doctorService;
    private $patientService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DoctorService $doctorService, PatientService $patientService)
    {
        $this->middleware('auth');
        $this->doctorService = $doctorService;
        $this->patientService = $patientService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $doctorExists = $this->doctorService->checkIfDoctorExists();
        
        if (!empty($doctorExists)) {
            $patientExists = $this->patientService->checkIfPatientExists($doctorExists->id);
        }
        else {
            $patientExists = null;
        }

        return view('home')->with('doctorExists',$doctorExists)->with('patientExists', $patientExists);
    }
}

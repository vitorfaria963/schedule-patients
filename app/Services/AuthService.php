<?php

namespace App\Services;

use App\Models\User;
use App\Exceptions\UserHasBeenTakenException;
use App\Exceptions\LoginInvalidException;

class AuthService
{
    /**
     * @param string $userName
     * @param string $email
     * @param string $password
     * @return mixed
     * @throws UserHasBeenTakenException
     */
    public function register(string $userName, string $email, string $password)
    {
      $user = User::where('email', $email)->exists();

      if(!empty($user)) {
        throw new UserHasBeenTakenException();
      }

      $userPwd = Self::encryptPassword($password);

      $user = User::create([
        'user_name' => $userName,
        'email'     => $email,
        'password'  => $userPwd
      ]);

      return $user;
    }

    public function login(string $email, string $password)
    {
        $loginData = [
          'email'    => $email,
          'password' => $password
        ];

        if (!auth()->attempt($loginData)) {
          throw new LoginInvalidException();
        } 

        $token = auth()->user()->createToken($email)->accessToken;
        return response()->json(['token' => $token], 200);

        // return [
        //     'token' => $token
        // ];
    }

    public static function encryptPassword(string $password)
    {
      return bcrypt($password);
    }
}
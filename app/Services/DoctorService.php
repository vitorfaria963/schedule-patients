<?php

namespace App\Services;

use App\Exceptions\CrmAlreadyExistsException;
use App\Models\Doctor;

class DoctorService
{

  public function checkIfDoctorExists()
  {
    $doctorHasProfile = Doctor::where('user_id', auth()->user()->id)->first();
    
    return $doctorHasProfile;
  }

  public function getDoctorLoggedIn(int $id)
  {
    $doctor = Doctor::where('user_id', '=', $id)->first();

    return $doctor;
  }

    /**
     * @param Doctor $doctor
     * @param array $input
     * @return Doctor|null
     * @throws CrmAlreadyExistsException
     */
    public function store(string $first_name, string $last_name, string $phone_number, string $crm)
    {
      $doctorCrm = Doctor::where('crm', $crm)->exists();
      if (!empty($doctorCrm)) {
        throw new CrmAlreadyExistsException();
      }

      // $doctor = auth()->user()->doctor()->create($input);
      $user_id = auth()->user()->id;
      
      $doctor = Doctor::create([
        'first_name' => $first_name,
        'last_name'  => $last_name,
        'phone_number' => $phone_number,
        'crm'          => $crm,
        'user_id'      => $user_id
      ]);
    
      return response()->json('Profile successfully created', 201);
    }
}
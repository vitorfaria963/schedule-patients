<?php

namespace App\Services;

use App\Exceptions\CpfAlreadyExistsException;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Schedule;

class ScheduleService
{
  public function getPatients()
  {
    $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();
    $doctor_id = isset($doctor_id) && !empty($doctor_id) ? $doctor_id->getAttributes()['id'] : null;
  
    $patients = !empty($doctor_id) ? Patient::with('doctor')->where('doctor_id', $doctor_id)->get() : null;
  
    if(count($patients) > 0){
        $arrData = $patients->toArray();
        foreach($arrData as $data)
        {
          $allPatients[] = $data;
        }
    }
    else {
        $allPatients = [];
    }

    return $allPatients;
  }

  public function getSchedules() 
  {
    $doctor = Doctor::where('user_id', '=', auth()->user()->id)->first();
    
    // Traz os dados do paciente pela relation com os dados "pivot" da tabela auxiliar
    if(count($doctor->schedules_patient) > 0){
     
      foreach($doctor->schedules_patient as $data)
      { 
        $allSchedules[] = $data->getOriginal();
      }
     }
     else {
      $allSchedules = [];
     }
  
    return $allSchedules;
  }

  public function getScheduledPatient(int $patient_id)
  {
    $patientData = Patient::find($patient_id);

    return $patientData;
  }

  public function store(int $patient_id, int $doctor_id, string $schedule_date, string $appointment_type)
  {
    
    $schedule = Schedule::create([
      'patient_id' => $patient_id,
      'doctor_id'  => $doctor_id,
      'schedule_date' => $schedule_date,
      'appointment_type' => $appointment_type
    ]);

    return response()->json('Schedule successfully created', 201);
  }
}
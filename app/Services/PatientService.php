<?php

namespace App\Services;

use App\Exceptions\CpfAlreadyExistsException;
use App\Models\Patient;
use App\Models\Doctor;

class PatientService
{
  public function checkIfPatientExists(int $doctor_id)
  {
    $doctor = Doctor::where('user_id', auth()->user()->id)->first();
    $patientExists = Patient::where('doctor_id', $doctor->id)->first();
    
    return $patientExists;
  }

  public function getPatients()
  {
    $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();
    $doctor_id = isset($doctor_id) && !empty($doctor_id) ? $doctor_id->getAttributes()['id'] : null;
  
    $patients = !empty($doctor_id) ? Patient::with('doctor')->where('doctor_id', $doctor_id)->get() : null;

    if(count($patients) > 0){
        $arrData = $patients->toArray();
        foreach($arrData as $data)
        {
          $allPatients[] = $data;
        }
    }
    else {
        $allPatients = [];
    }

    return $allPatients;
  }
  
    /**
     * @param Patient $doctor
     * @param array $input
     * @return Patient|null
     * @throws CpfAlreadyExistsException
     */
    public function store(array $input)
    {
      $patientCpf = Patient::where('cpf', $input['cpf'])->exists();
      if (!empty($patientCpf)) {
        throw new CpfAlreadyExistsException();
      }

      $doctor = Doctor::where('user_id', '=', auth()->user()->id)->first();
      $doctor->patients()->create($input);

      return response()->json('Patient successfully created', 201);
    }
}
<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Doctor;
use Illuminate\Auth\Access\HandlesAuthorization;

class DoctorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Doctor $doctor
     * @return bool
     */
    public function view(User $user, Doctor $doctor)
    {
        return $user->id === $doctor->user_id;
    }

    /**
     * @param User $user
     * @param Doctor $doctor
     * @return bool
     */
    public function update(User $user, Doctor $doctor)
    {
        return $user->id === $doctor->user_id;
    }

    /**
     * @param User $user
     * @param Doctor $doctor
     * @return bool
    */
    public function destroy(User $user, Doctor $doctor)
    {
        return $user->id === $doctor->user_id;
    }
    
}

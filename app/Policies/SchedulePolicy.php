<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Doctor; 
use App\Models\Schedule;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchedulePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Schedule $schedule
     * @return bool
     */
    public function view(User $user, Schedule $schedule)
    {  
        $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();

        if(!empty($doctor_id)) {
            $doctor_id = $doctor_id->getAttributes()['id'];
        }
       
        return $doctor_id === $schedule->doctor_id;
    }

     /**
     * @param User $user
     * @param Patient $patient
     * @return bool
     */
    public function update(User $user, Schedule $schedule)
    {
        $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();
        if (!empty($doctor_id)) {
            $doctor_id = $doctor_id->getAttributes()['id'];
        }

        return $doctor_id === $schedule->doctor_id;
    }

    /**
     * @param User $user
     * @param Schedule $schedule
     * @return bool
     */
    public function destroy(User $user, Schedule $schedule)
    {
        $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();
        if (!empty($doctor_id)) {
            $doctor_id = $doctor_id->getAttributes()['id'];
        }
        
        return $doctor_id === $schedule->doctor_id;
    }
}

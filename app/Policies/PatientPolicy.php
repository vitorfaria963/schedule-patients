<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Doctor;
use App\Models\Patient;
use Illuminate\Auth\Access\HandlesAuthorization;

class PatientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Patient $patient
     * @return bool
     */
    public function view(User $user, Patient $patient)
    {
        $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();

        if(!empty($doctor_id)) {
           $doctor_id =  $doctor_id->getAttributes()['id'];
        }

        return $doctor_id === $patient->doctor_id;
    }

    /**
     * @param User $user
     * @param Patient $patient
     * @return bool
     */
    public function update(User $user, Patient $patient)
    {
        $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();

        if(!empty($doctor_id)) {
            $doctor_id = $doctor_id->getAttributes()['id'];
        }

        return $doctor_id === $patient->doctor_id;
    }

    /**
     * @param User $user
     * @param Patient $patient
     * @return bool
     */
    public function destroy(User $user, Patient $patient)
    {
        $doctor_id = Doctor::where('user_id', '=', auth()->user()->id)->first();
        if(!empty($doctor_id)) {
            $doctor_id = $doctor_id->getAttributes()['id'];
        }

        return $doctor_id === $patient->doctor_id;
    }
}

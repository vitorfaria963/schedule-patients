# schedule-patients

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!



## Installation
Após clonar o repositório do projeto, deve ser utilizado os seguintes comandos para instalação do projeto para que rode localmente.


```bash
$ composer update

$ npm install

$npm run dev
```
## running project

Após ter instalado as dependências do laravel com composer update, ter instalado node_modules pelo npm e ter sincronizado para fazer o build do boostrap 4 rodando como Laravel Mix, é necessário subir os containers do banco utilizando o docker com o comando: docker-compose up -d

Logo após isso, rodar o comando: php artisan migrate para que as tabelas sejam migradas para o banco servido pelo docker

Após subir os containers, é necessário fazer a instalação do passport para que seja gerado as 2 keys criptografadas por ele
Comando para instalação do passport: php artisan passport:install

```bash
Após ter finalizado o processo de instalação, é necessário inicializar o projeto utilizando o comando: php artisan serve
```

## authentication
O projeto roda com serviço de autenticação. Usuário deve fazer o cadastro e fazer login na plataforma com o token gerado na request do login, utilizando este token em todas as rotas de 'navegação' da API
